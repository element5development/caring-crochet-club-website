<?php 
/*-------------------------------------------------------------------
    Template Name: Landing/Marketing
-------------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/pages/header-page'); ?>

<?php get_template_part('template-parts/navigation/anchor'); ?>

<?php if( !empty(get_the_content()) ) { ?>
	<section class="default-contents">
		<div  id="default" class="anchor"></div>
		<div class="block">
			<?php get_template_part('template-parts/pages/content', 'default'); ?>
		</div>
	</section>
<?php } ?>

<?php get_template_part('template-parts/elements/sticky-deal'); ?>

<?php get_template_part('template-parts/elements/split-cta'); ?>

<?php get_template_part('template-parts/elements/join'); ?>

<?php get_template_part('template-parts/elements/testimonies'); ?>

<?php } ?>

<?php get_footer(); ?>