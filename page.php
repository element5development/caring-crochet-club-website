<?php 
/*-------------------------------------------------------------------

Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vel
vestibulum erat. Aliquam iaculis lectus sit amet lorem posuere, at
feugiat arcu imperdiet. Nullam tempor, purus quis aliquam luctus,
purus nulla lobortis diam, eget posuere massa quam a diam. Duis
dignissim velit neque, sed faucibus nulla luctus vitae.  

------------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/pages/header-page'); ?>

<?php get_template_part('template-parts/navigation/anchor'); ?>

<main>
	<article>
		<div id="default" class="anchor"></div>

		<?php if( !empty(get_the_content()) ) { ?>
			<section class="default-contents">
				<div class="block">
					<?php get_template_part('template-parts/pages/content', 'default'); ?>
				</div>
			</section>
		<?php } ?>

		<?php get_template_part('template-parts/elements/annies-kits'); ?>
		
	</article>
</main>

<?php get_footer(); ?>