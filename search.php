<?php 
/*-------------------------------------------------------------------

Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vel
vestibulum erat. Aliquam iaculis lectus sit amet lorem posuere, at
feugiat arcu imperdiet. Nullam tempor, purus quis aliquam luctus,
purus nulla lobortis diam, eget posuere massa quam a diam. Duis
dignissim velit neque, sed faucibus nulla luctus vitae.  

------------------------------------------------------------------*/
?>

<?php
// store the post type from the URL string
$post_type = $_GET['post_type'];
// check to see if there was a post type in the
// URL string and if a results template for that
// post type actually exists
if ( isset( $post_type ) && locate_template( 'search-' . $post_type . '.php' ) ) {
		// if so, load that template
		remove_filter('posts_request', 'relevanssi_prevent_default_request'); 
		remove_filter('the_posts', 'relevanssi_query', 99);
    get_template_part( 'search', $post_type );

    // and then exit out
    exit;
}
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/pages/header-archive'); ?>

<?php get_template_part('template-parts/navigation/anchor'); ?>

<main>
	<article>
		<div id="default" class="anchor"></div>
	
		<section class="search-feed feed default-contents">
			<div class="block">
				<?php get_search_form(); ?>

				<h2>Search Results for '<?php echo get_search_query() ?>'</h2>

				<?php if (!have_posts()) : ?>
					<h3>Sorry, no results were found</h3>
				<?php endif; ?>

				<?php while (have_posts()) : the_post(); ?>
					<?php get_template_part('template-parts/posts/previews/preview', 'search'); ?>
				<?php endwhile; ?>
			</div>
			<?php
				the_posts_pagination( array(
					'prev_text'	=> __( 'Previous Page' ),
					'next_text'	=> __( 'Next Page' ),
				) );
			?>
		</section>

	</article>
</main>

<?php get_footer(); ?>