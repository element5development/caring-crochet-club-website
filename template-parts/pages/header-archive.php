<?php 
/*-------------------------------------------------------------------

Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vel
vestibulum erat. Aliquam iaculis lectus sit amet lorem posuere, at
feugiat arcu imperdiet. Nullam tempor, purus quis aliquam luctus,
purus nulla lobortis diam, eget posuere massa quam a diam. Duis
dignissim velit neque, sed faucibus nulla luctus vitae.  

------------------------------------------------------------------*/
?>

<?php 
	if ( get_field('background_image') ) {
		$background = get_field('background_image');
		$backgroundURL = $background['url'];
	} else {
		$backgroundURL = get_stylesheet_directory_uri() . '/dist/images/default-header.jpg';
	}
?>

<section class="archive-title page-title title-section" style="background-image: url(<?php echo $backgroundURL; ?>);">
	<div class="block">
		<?php if ( is_search() ) { ?>
			<h1>Search: <?php the_search_query(); ?></h1>
		<?php } else if ( is_home() )  { ?>
			<h1>Blog</h1>
		<?php } else if ( is_archive() )  { ?>
			<h1><?php the_archive_title() ?></h1>
		<?php } else { } ?>
	</div>
</section>