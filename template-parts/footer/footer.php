<?php 
/*-------------------------------------------------------------------

Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vel
vestibulum erat. Aliquam iaculis lectus sit amet lorem posuere, at
feugiat arcu imperdiet. Nullam tempor, purus quis aliquam luctus,
purus nulla lobortis diam, eget posuere massa quam a diam. Duis
dignissim velit neque, sed faucibus nulla luctus vitae.  

------------------------------------------------------------------*/
?>

<footer class="site-footer">
	<?php get_template_part('template-parts/navigation/secondary'); ?>
	<?php get_template_part('template-parts/navigation/badges'); ?>
	<?php get_template_part('template-parts/navigation/social'); ?>
	<p>©<?php echo date('Y'); ?> <?php echo get_bloginfo( 'name' ); ?>. All rights reserved. Caring Crochet Kit Club is a trademark of <a target="_blank" href="https://www.annieskitclubs.com/">Annie’s®</a></p>
	<div id="element5-credit">
		<a target="_blank" href="https://element5digital.com">
			<img src="https://element5digital.com/wp-content/themes/e5-starting-point/dist/images/element5_credit.svg" />
		</a>
	</div>
</footer>