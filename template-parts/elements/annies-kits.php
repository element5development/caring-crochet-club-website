
<?php if( have_rows('annie_kits') ): ?>
	<hr>
	<section class="annie-kits-block">
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/annies-logo.svg" alt="annies logo" />
		<h3>Customers who purchased the Caring Crochet Kit also bought:</h3>
		<?php $count = count(get_field('annie_kits')); ?>
		<div class="annie-kits four-col">
			<?php while ( have_rows('annie_kits') ) : the_row(); ?>
				<?php
					$image = get_sub_field('image');
					$alt = $image['alt'];
					$preview = $image['sizes']['small'];
					$link = get_sub_field('button');
				?>
				<a href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>">
					<div class="annie-kit">
						<img src="<?php echo $preview; ?>" alt="<?php echo $alt; ?>" />
						<h4><?php the_sub_field('title'); ?></h4>
						<!-- <p class="price">$<?php the_sub_field('price'); ?>/kit</p> -->
						<div class="button" href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>"><?php echo $link['title']; ?></div>
					</div>
				</a>
			<?php endwhile; ?>
		</div>
	</section>
<?php endif; ?>