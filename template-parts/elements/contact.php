<?php 
/*-------------------------------------------------------------------

Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vel
vestibulum erat. Aliquam iaculis lectus sit amet lorem posuere, at
feugiat arcu imperdiet. Nullam tempor, purus quis aliquam luctus,
purus nulla lobortis diam, eget posuere massa quam a diam. Duis
dignissim velit neque, sed faucibus nulla luctus vitae.  

------------------------------------------------------------------*/
?>

<section class="contact">
	<div  id="guarantee" class="anchor"></div>
	<div class="guarantee">
		<div class="block">
			<h2>Our Guarantee</h2>
			<?php the_field('guarantee', 'options'); ?>
		</div>
	</div>
	<div class="contact-details">
		<?php 
			$phone_link = get_field('primary_phone','option');
			$phone_link = preg_replace('/[^0-9]/', '', $phone_link);
			$join_link = get_field('join_url','option');
		?>
		<div class="block">
			<h2>Contact Us</h2>
			<p><a target="<?php echo $join_link['target']; ?>" href="<?php echo $join_link['url']; ?>">Order Online</a> or call us toll-free <a href="tel:+1<?php echo $phone_link; ?>"><?php the_field('primary_phone','options'); ?></a></p>
			<?php the_field('primary_hours', 'option'); ?>
			<a href="https://csportal.anniescustomercare.com/contact_us.php?program_id=CKC" class="button">contact us</a>
		</div>
	</div>
</section>