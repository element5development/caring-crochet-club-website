<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>"> 
  <input type="search" class="search-field" 
    placeholder="<?php echo esc_attr_x( 'Search for a kit...', 'placeholder' ) ?>" 
    value="<?php echo get_search_query() ?>" name="s" 
    title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>" /> 
  <a href="<?= esc_url(home_url('/')); ?>help/pattern-corrections/">Clear</a> 
  <input type="hidden" name="post_type" value="kit" /> 
  <button type="submit" class="search-submit" value="<?php echo esc_attr_x( 'Search', 'submit button' ) ?>">Search</button> 
</form>