<?php 
/*-------------------------------------------------------------------

Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vel
vestibulum erat. Aliquam iaculis lectus sit amet lorem posuere, at
feugiat arcu imperdiet. Nullam tempor, purus quis aliquam luctus,
purus nulla lobortis diam, eget posuere massa quam a diam. Duis
dignissim velit neque, sed faucibus nulla luctus vitae.  

------------------------------------------------------------------*/
?>


<nav class="nav-anchor">
	<?php if (has_nav_menu('primary_nav')) :
		wp_nav_menu(['theme_location' => 'primary_nav']);
	endif; ?>
</nav>
<button id="menu-toggle">
	<svg width="30" height="25" viewBox="0 0 30 25">
		<path d="M0 10h30v5H0v-5zM0 0h30v5H0V0zm0 20h30v5H0v-5z" fill="#454148" fill-rule="evenodd"/>
	</svg>
	<svg width="28" height="27" viewBox="0 0 28 27">
		<path d="M17.61 13.433l9.681 9.681-3.818 3.819-9.682-9.682-9.5 9.5-3.818-3.818 9.5-9.5-9.41-9.41L4.38.204l9.41 9.41L23.383.023 27.2 3.84l-9.592 9.592z" fill="#454148" fill-rule="evenodd"/>
	</svg>
</button>