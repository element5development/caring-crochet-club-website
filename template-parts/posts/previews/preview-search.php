<?php 
/*-------------------------------------------------------------------

Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vel
vestibulum erat. Aliquam iaculis lectus sit amet lorem posuere, at
feugiat arcu imperdiet. Nullam tempor, purus quis aliquam luctus,
purus nulla lobortis diam, eget posuere massa quam a diam. Duis
dignissim velit neque, sed faucibus nulla luctus vitae.  

------------------------------------------------------------------*/
?>
<?php if(get_post_type() == 'kit'): ?>
	<article class="post-preview-kit">

		<?php
		if( have_rows('instruction_documentation') ):
			while ( have_rows('instruction_documentation') ) : the_row();
				$title = get_sub_field('title');
				$number = get_sub_field('revision_number');
				$download = get_sub_field('download');
				?>
				<a href="<?php echo $download; ?>">
					<div class="pdf-card">
						<svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 130.2 120">
							<defs>
								<style>
									.cls-24{fill:none;stroke:#454148;stroke-width:2px;stroke-linecap:round;stroke-linejoin:round}.cls-48{fill:#fff}
								</style>
							</defs>
							<path fill="#454148" d="M24.57 5.11h75.76v29.26H24.57z"/>
							<path class="cls-24" d="M78.05 105.78H24.57V5.11h75.77v78.71l-22.29 21.96z"/>
							<path class="cls-24" d="M100.42 13.36h7.4v100.67H32.06v-8.17"/>
							<path class="cls-24" d="M100.34 83.82l-22.29 21.96V83.82h22.29z"/>
							<path stroke-miterlimit="10" fill="none" stroke="#454148" stroke-width="2" d="M55.4665 45.3849l14.05-.0393.049 17.56-14.05.0392z"/>
							<path class="cls-24" d="M69.81 50.08s9.66-6.52 16.69-4.19c5.86 2.32 3.53 7 3.53 7"/>
							<path class="cls-24" d="M88.88 61.1c4.69-1.18 4.66-10.55-5.88-10.52a15.56 15.56 0 0 0-7.9 2.14M83 45.31s-3.75-4.23-13.47 0"/>
							<path class="cls-24" d="M86.4 54.7c-1.19 2-8.35 4.53-16.86-.57 0 0 4.1-2.35 12.3-1.2s9.39 9.34 3.54 11.7-15.23-1.72-15.23-1.72M88.74 61c-5.24.74-12.88.32-19.19-3.17M55.52 63c-3.41 1.55-9.37 1.78-12.88 1.35S33.85 61.4 33.83 56s10.56-8.7 17.88-7.7"/>
							<path class="cls-24" d="M55.47 45.39S46.24 43 41.85 43.54s-9.63 3.6-7.68 10.69M55.49 51.39a28.5 28.5 0 0 0-15-7.61M55.5 57.83A118.33 118.33 0 0 1 33.83 56M55.49 53c-3.81 0-16.85-.49-21.53 1.93M39.77 56.93c1.39 2.7 5.72 4.88 15.74 4.26"/>
							<rect class="cls-24" x="51.88" y="60.01" width="5.15" height="25.81" rx="2.57" ry="2.57" transform="rotate(89.5 54.4469 72.911)"/>
							<path class="cls-24" d="M67.24 71.65l8.34-.07c4.69 0 6.39.79 6.31 1.82s-4.41 1.26-4.41 1.26a2.38 2.38 0 0 1 .79-.83c.47-.24.52-.54.44-.73s-.61-.16-1.48 0a38.91 38.91 0 0 1-4.39.4c-1.57.06-1.87.09-5.48.17"/>
							<path class="cls-48" d="M54.19 17.88a4.5 4.5 0 0 1-1.49 3.63 6.33 6.33 0 0 1-4.22 1.26h-1.34V28H44V13.29h4.7a6.29 6.29 0 0 1 4.07 1.15 4.22 4.22 0 0 1 1.42 3.44zm-7 2.34h1a3.39 3.39 0 0 0 2.15-.57A2 2 0 0 0 51 18a2 2 0 0 0-.6-1.62 2.81 2.81 0 0 0-1.88-.52h-1.38zM69.79 20.51a7.24 7.24 0 0 1-2.07 5.56 8.42 8.42 0 0 1-6 1.93h-4.14V13.29h4.62a7.83 7.83 0 0 1 5.6 1.9 7 7 0 0 1 1.99 5.32zm-3.24.08q0-4.74-4.19-4.74H60.7v9.58H62c3.05 0 4.55-1.62 4.55-4.84zM76.53 28h-3.07V13.29h8.43v2.56h-5.36v3.79h5v2.55h-5z"/>
						</svg>
						<h5><?php echo $title; ?></h5>
						<p>Corrections <?php echo $number; ?></p>
						<p>(PDF))</p>
						<a href="<?php echo $download; ?>" class="button">Download</a>
					</div>
				</a>
				<?php
			endwhile;
		else :
			// no rows found
		endif;
		?>

	</article>
	<hr>
	<?php else: ?>
	<a href="<?php the_permalink(); ?>">
		<article class="post-preview">
			<header>
				<h3><?php the_title(); ?></h3>
			</header>
			<?php the_excerpt(); ?>
		</article>
	</a>
	<hr>
<?php endif; ?>