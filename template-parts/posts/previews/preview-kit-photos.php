<?php 
/*-------------------------------------------------------------------

Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vel
vestibulum erat. Aliquam iaculis lectus sit amet lorem posuere, at
feugiat arcu imperdiet. Nullam tempor, purus quis aliquam luctus,
purus nulla lobortis diam, eget posuere massa quam a diam. Duis
dignissim velit neque, sed faucibus nulla luctus vitae.  

------------------------------------------------------------------*/
?>
<article class="post-preview-photos">

	<?php
	if( have_rows('gallery') ):
		$i = 0;
		while ( have_rows('gallery') ) : the_row(); ?>
		<?php $i++; ?>
		<?php if( $i > 3 ): ?>
			<?php break; ?>
		<?php endif; ?>
		<?php 
			$image = get_sub_field('image');
			$alt = $image['alt'];
			$preview = $image['sizes']['medium'];
			?>
			<a href="<?php echo $image['url']; ?>" class="gallery-image <?php the_sub_field('size'); ?> <?php echo $post->post_name; ?>">
				<div class="preview-image" style="background-image: url(<?php echo $image['url']; ?>);">
					<img src="<?php echo $preview; ?>" alt="<?php echo $image['alt']; ?>" />
					<div class="overlay"></div>
					<p><?php the_title(); ?><br>
						<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="43" height="44">
							<defs>
								<path id="w" d="M933.18 1447.37a16.18 16.18 0 1 1 0-32.37 16.18 16.18 0 0 1 0 32.37z"/>
								<path id="x" d="M943.73 1442.83l11.97 11.97"/>
								<path id="y" d="M933.18 1423.32v16.22"/>
								<path id="z" d="M925.08 1431.43h16.22"/>
							</defs>
							<use fill="#fff" fill-opacity="0" stroke="#fff" stroke-miterlimit="50" stroke-width="4" xlink:href="#w" transform="translate(-915 -1413)"/>
							<use fill="#fff" fill-opacity="0" stroke="#fff" stroke-miterlimit="50" stroke-width="6" xlink:href="#x" transform="translate(-915 -1413)"/>
							<use fill="#fff" fill-opacity="0" stroke="#fff" stroke-miterlimit="50" stroke-width="3" xlink:href="#y" transform="translate(-915 -1413)"/>
							<use fill="#fff" fill-opacity="0" stroke="#fff" stroke-miterlimit="50" stroke-width="3" xlink:href="#z" transform="translate(-915 -1413)"/>
						</svg>
					</p>
				</div>
			</a>
			<?php
		endwhile;
	else :
		// no rows found
	endif;
	?>

</article>